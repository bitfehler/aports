# Contributor: TBK <alpine@jjtc.eu>
# Maintainer: TBK <alpine@jjtc.eu>
pkgname=php7-pecl-protobuf
_extname=protobuf
pkgver=3.21.1
pkgrel=0
pkgdesc="PHP 7 extension: Google's language-neutral, platform-neutral, extensible mechanism for serializing structured data."
url="https://pecl.php.net/package/protobuf"
arch="all"
license="BSD-3-Clause"
depends="php7-common"
makedepends="php7-dev"
source="php-pecl-$_extname-$pkgver.tgz::https://pecl.php.net/get/$_extname-$pkgver.tgz"
builddir="$srcdir/$_extname-$pkgver"
provides="php7-protobuf=$pkgver-r$pkgrel" # for backward compatibility
replaces="php7-protobuf" # for backward compatibility

build() {
	phpize7
	./configure --prefix=/usr --with-php-config=php-config7
	make
}

check() {
	# Test suite is not a part of pecl release.
	php7 -d extension=modules/$_extname.so --ri $_extname
}

package() {
	make INSTALL_ROOT="$pkgdir" install
	local _confdir="$pkgdir"/etc/php7/conf.d
	install -d $_confdir
	echo "extension=$_extname" > $_confdir/$_extname.ini
}

sha512sums="
ebc1f0ed219ebdeeb110b16656c232fb04ac9dd7e3a6a6585d7be88437b1e127853086d18bab4a39a16a42557aef3b9c1e4a6a19e5e1fa0389c9c3a8cb9ab59a  php-pecl-protobuf-3.21.1.tgz
"
