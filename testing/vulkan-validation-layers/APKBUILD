# Contributor: Simon Zeni <simon@bl4ckb0ne.ca>
# Maintainer: Simon Zeni <simon@bl4ckb0ne.ca>
pkgname=vulkan-validation-layers
pkgver=1.3.211.0
pkgrel=0
pkgdesc="Vulkan Validation Layers"
url="https://www.khronos.org/vulkan/"
arch="all"
license="Apache-2.0"
makedepends="
	cmake
	libx11-dev
	libxcb-dev
	libxrandr-dev
	ninja
	python3
	robin-hood-hashing
	spirv-headers
	spirv-tools-dev
	vulkan-headers
	wayland-dev
	"
subpackages="$pkgname-dbg $pkgname-static $pkgname-dev"
source="$pkgname-$pkgver.tar.gz::https://github.com/KhronosGroup/Vulkan-ValidationLayers/archive/refs/tags/sdk-$pkgver.tar.gz"
builddir="$srcdir/Vulkan-ValidationLayers-sdk-$pkgver"
options="!check" # test segfaults

build() {
	cmake -B build -G Ninja -Wno-dev \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DCMAKE_INSTALL_DATAROOTDIR=/usr/share \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DBUILD_LAYER_SUPPORT_FILES=ON \
		-DBUILD_WSI_XCB_SUPPORT=ON \
		-DBUILD_WSI_XLIB_SUPPORT=ON \
		-DBUILD_WSI_WAYLAND_SUPPORT=ON \
		-DBUILD_WERROR=OFF

	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
e494ba793064f0d32fbdeaddaa01dcd730a36a5c793625bb4671a4e8067364171416ec8fb7361f946cc5d5e8dc16d58e2194c3eb5d3a57d58f4ace2e7ed34f5f  vulkan-validation-layers-1.3.211.0.tar.gz
"
