# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=eventviews
pkgver=22.04.1
pkgrel=0
pkgdesc="Library for creating events"
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by polkit -> akonadi
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://kontact.kde.org/"
license="GPL-2.0-or-later"
depends_dev="
	akonadi-calendar-dev
	akonadi-dev
	calendarsupport-dev
	kcalendarcore-dev
	kcalutils-dev
	kcodecs-dev
	kcompletion-dev
	kdiagram-dev
	kguiaddons-dev
	kholidays-dev
	ki18n-dev
	kiconthemes-dev
	kmime-dev
	kservice-dev
	libkdepim-dev
	qt5-qtbase-dev
	samurai
	"
checkdepends="xvfb-run"
makedepends="$depends_dev extra-cmake-modules"
source="https://download.kde.org/stable/release-service/$pkgver/src/eventviews-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
1ed06057ba75ffd751a37e839b0660347c70b0f18c304a993a5c20ce623fcf49ad984582a95f1d688fae93b3140ee596ba2605401fa70e9101663371a9d366f1  eventviews-22.04.1.tar.xz
"
