# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kalarm
pkgver=22.04.1
pkgrel=0
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by polkit -> kio
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://kontact.kde.org/"
pkgdesc="Personal alarm scheduler"
license="GPL-2.0-or-later AND GFDL-1.2-only"
depends="kdepim-runtime"
makedepends="
	akonadi-contacts-dev
	akonadi-dev
	akonadi-mime-dev
	extra-cmake-modules
	kauth-dev
	kcalendarcore-dev
	kcalutils-dev
	kcmutils-dev
	kcodecs-dev
	kcompletion-dev
	kconfig-dev
	kconfigwidgets-dev
	kdbusaddons-dev
	kdelibs4support-dev
	kdoctools-dev
	kglobalaccel-dev
	kguiaddons-dev
	kholidays-dev
	ki18n-dev
	kiconthemes-dev
	kidentitymanagement-dev
	kidletime-dev
	kimap-dev
	kio-dev
	kjobwidgets-dev
	kmailtransport-dev
	kmime-dev
	knotifications-dev
	knotifyconfig-dev
	kpimtextedit-dev
	kservice-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	kxmlgui-dev
	libkdepim-dev
	libxslt-dev
	mailcommon-dev
	phonon-dev
	pimcommon-dev
	qgpgme
	qt5-qtbase-dev
	qt5-qtx11extras-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kalarm-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # Tests are broken

build() {
	cmake -B build -G Ninja\
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
ffccf5ca223ded3a386fe7b8384e9b05d81c5e8d1b5d723d5ad59e3f519c2a46b1f59b0876233b52eb969ca3e5c9fb4575b220980940bf2f7245311894f30fd5  kalarm-22.04.1.tar.xz
"
