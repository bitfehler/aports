# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kde-dev-scripts
pkgver=22.04.1
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf !s390x"
url="https://kde.org/applications/development/"
pkgdesc="Scripts and setting files useful during development of KDE software"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kdoctools-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kde-dev-scripts-$pkgver.tar.xz"
subpackages="$pkgname-doc"
options="!check" # No code to test

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="
b9d617aa306a2b7780bd1139aa9886958a6805384dc33f1810d6d07d14bdf10dfdda9559803035fa167ff573d44d7f6774d3fe68e174c1227eedb3fe3ad3db49  kde-dev-scripts-22.04.1.tar.xz
"
