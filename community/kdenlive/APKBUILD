# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kdenlive
pkgver=22.04.1
pkgrel=0
# s390x and riscv64 blocked by polkit -> kxmlgui
# ppc64le mlt uses 64bit long double while libgcc uses 128bit long double
arch="all !armhf !s390x !ppc64le !riscv64"
url="https://kdenlive.org"
pkgdesc="An intuitive and powerful multi-track video editor, including most recent video technologies"
license="GPL-2.0-or-later"
depends="
	ffmpeg
	frei0r-plugins
	qt5-qtquickcontrols
	"
makedepends="
	extra-cmake-modules
	kdeclarative-dev
	kdoctools-dev
	kfilemetadata-dev
	knewstuff-dev
	knotifyconfig-dev
	kxmlgui-dev
	mlt-dev
	purpose-dev
	qt5-qtbase-dev
	qt5-qtmultimedia-dev
	qt5-qtnetworkauth-dev
	qt5-qtsvg-dev
	rttr-dev
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/release-service/$pkgver/src/kdenlive-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # Broken

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
9b12a7809bc68297a57361e4523c2657986d152cfacf4f3ee24be018d763a9c80407eb13fe4e98f29e3543e521d82f9caf0a9f3d813fac3c6aa585b51d5fbcda  kdenlive-22.04.1.tar.xz
"
