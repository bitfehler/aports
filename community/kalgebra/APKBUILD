# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kalgebra
pkgver=22.04.1
pkgrel=0
# armhf blocked by qt5-qtdeclarative
# s390x and riscv64 blocked by polkit -> kio
arch="all !armhf !s390x !riscv64"
url="https://edu.kde.org/kalgebra/"
pkgdesc="2D and 3D Graph Calculator"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	analitza-dev
	extra-cmake-modules
	kconfigwidgets-dev
	kcoreaddons-dev
	kdoctools-dev
	ki18n-dev
	kio-dev
	kwidgetsaddons-dev
	ncurses-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtsvg-dev
	readline-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kalgebra-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
67274bb344ac7b07d814ec3b4c14c7a8913601ced784a90498ede2f0d12b028c9bd612bee02a2de08c5c0fe89dfabfe9e2d2160f378eba3dd868463041d51c45  kalgebra-22.04.1.tar.xz
"
