# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kwalletmanager
pkgver=22.04.1
pkgrel=0
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit -> kio
arch="all !armhf !s390x !riscv64"
url="https://kde.org/applications/system/org.kde.kwalletmanager5"
pkgdesc="A tool to manage the passwords on your KDE system"
license="GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	karchive-dev
	kauth-dev
	kcmutils-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kjobwidgets-dev
	knotifications-dev
	kservice-dev
	ktextwidgets-dev
	kwallet-dev
	kwindowsystem-dev
	kxmlgui-dev
	qt5-qtbase-dev
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/release-service/$pkgver/src/kwalletmanager-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
0296e5496369e639d499a47b01123800375bc23ab5953ac7b21ddf786f347078b7527f38e2443e2152c66cbe507e21683ac32579bab2c3e237d3e250023c3566  kwalletmanager-22.04.1.tar.xz
"
