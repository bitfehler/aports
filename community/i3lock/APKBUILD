# Contributor: Johannes Matheis <jomat+alpinebuild@jmt.gr>
# Maintainer: Johannes Matheis <jomat+alpinebuild@jmt.gr>
pkgname=i3lock
pkgver=2.14
pkgrel=0
pkgdesc="An improved screenlocker based upon XCB and PAM"
url="https://i3wm.org/i3lock/"
arch="all"
license="MIT"
depends="xkeyboard-config"
makedepends="
	cairo-dev
	libev-dev
	libxkbcommon-dev
	linux-pam-dev
	meson
	xcb-util-image-dev
	xcb-util-xrm-dev
	"
subpackages="$pkgname-doc"
source="https://i3wm.org/i3lock/i3lock-$pkgver.tar.xz"

prepare() {
	default_prepare

	# Fix ticket FS#31544, sed line taken from gentoo
	sed -i -e 's:login:base-auth:g' pam/i3lock
}

build() {
	abuild-meson . output
	meson compile -C output
}

check() {
	./output/i3lock -v
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

sha512sums="
85f755333739e61a8ac90447410c48cf54345740bb6c6977efa88500e2b66dfd7fe3338e4fe38f817773219e5994ef6375010fdb3cf711e2ad42045874e39e20  i3lock-2.14.tar.xz
"
