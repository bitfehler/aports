# Contributor: Bart Ribbers <bribbers@disroot.org>
# Contributor: Simon Zeni <simon@bl4ckb0ne.ca>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=vulkan-loader
pkgver=1.3.211.0
pkgrel=0
arch="all"
url="https://www.khronos.org/vulkan"
pkgdesc="Vulkan Installable Client Driver (ICD) Loader"
license="Apache-2.0"
depends_dev="vulkan-headers"
makedepends="$depends_dev
	cmake
	libx11-dev
	libxrandr-dev
	python3
	samurai
	wayland-dev
	"
source="https://github.com/khronosgroup/vulkan-loader/archive/sdk-$pkgver/vulkan-loader-sdk-$pkgver.tar.gz"
subpackages="$pkgname-dev $pkgname-dbg"
options="!check" # No tests
builddir="$srcdir/Vulkan-Loader-sdk-$pkgver"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DCMAKE_INSTALL_SYSCONFDIR=/etc \
		-DCMAKE_INSTALL_DATADIR=share \
		-DCMAKE_SKIP_RPATH=True \
		-DVULKAN_HEADERS_INSTALL_DIR=/usr
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
217f3cd9a9d9448815044321eb1a29a516cf7b5532eb7da326bb4dc3a8867068836b9e50caae7c0a02bf8632c5dd9b4dab3a5a7a1d304f01e662e4650fdb1062  vulkan-loader-sdk-1.3.211.0.tar.gz
"
