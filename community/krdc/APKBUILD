# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=krdc
pkgver=22.04.1
pkgrel=0
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit -> kxmlgui
arch="all !armhf !s390x !riscv64"
url="https://kde.org/applications/internet/krdc/"
pkgdesc="Remote Desktop Client"
license="GPL-2.0-or-later AND GFDL-1.2-only"
depends="freerdp"
makedepends="
	extra-cmake-modules
	kbookmarks-dev
	kcmutils-dev
	kcompletion-dev
	kconfig-dev
	kdnssd-dev
	kdoctools-dev
	ki18n-dev
	kiconthemes-dev
	knotifications-dev
	knotifyconfig-dev
	knotifyconfig-dev
	kwallet-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	kxmlgui-dev
	libssh-dev
	libvncserver-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/krdc-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
ee27af1e81f63b3d977d1615929a1de6f6f7b67879e8b0683186fca72cdab54c6311eae90cdb32f78e078280ea5526d250cf361703a60ae7687ee6c163745bef  krdc-22.04.1.tar.xz
"
