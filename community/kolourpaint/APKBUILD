# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kolourpaint
pkgver=22.04.1
pkgrel=0
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by polkit -> kio
arch="all !armhf !s390x !riscv64"
url="http://www.kolourpaint.org/"
pkgdesc="An easy-to-use paint program"
license="BSD-2-Clause AND LGPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kdelibs4support-dev
	kdoctools-dev
	kguiaddons-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kitemmodels-dev
	ktextwidgets-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	libksane-dev
	qt5-qtbase-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kolourpaint-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
1cf4e03ada2d4b54db533ac46589dddd1f11e72a558f1cc1d633644d1d0718dd52cfc7cb14d07960e68eed5b34e39d582b3549eaf9e87c33d8a9dffcc0c8b9ca  kolourpaint-22.04.1.tar.xz
"
